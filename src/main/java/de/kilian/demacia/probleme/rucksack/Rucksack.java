package de.kilian.demacia.probleme.rucksack;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Rucksack {

    private int rucksackplatz; // Rucksackplatz in zb Kg

    // Konstruktor, der ein leeres Inventar mit einer bestimmten Größe erstellt
    public Rucksack(int rucksackplatz) {
        this.rucksackplatz = rucksackplatz;
    }

}