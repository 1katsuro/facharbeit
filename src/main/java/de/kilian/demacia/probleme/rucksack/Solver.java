package de.kilian.demacia.probleme.rucksack;

import de.kilian.demacia.datenstrukturen.BinaryTree;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Solver {

    //Variable die für den Backtracking Algorithmus benötigt wird
    private int hoechsterWertB = 0;

    //Variablen für Verdeutlichung der Unterschiede zwischen den Algorithmen
    private int versuchteKombinationen = 0;

    private List<Item[]> ConquerBaumInListenform = new ArrayList<>();


    private int[][] matrix;
    private int[][] besteKombinationMatrix;

    private BinaryTree<Item[]> tree;

    private Item[] sortieteItems;

    //Variablen die für die Statistik benötigt werden
    private long zeitZumLoesen = 0;
    private Item[] sortierteItems;

    // Brute-Force Algorithmus
    public Item[] algorithm1(Rucksack rucksack, List<Item> items) {
        Item[] besteKombination = null;
        int hoechsterWert = 0;

        int n = items.size();
        String format = "%" + n + "s";

        //Statistik
        versuchteKombinationen = 0;
        long startZeit = System.currentTimeMillis();

        for (int i = 0; i < Math.pow(2, n); i++) {
            Item[] aktuelleKombination = new Item[n];
            int totalerWert = 0;
            int totalesGewicht = 0;

            //Statistik
            versuchteKombinationen++;

            String binaryString = String.format(format,
                    Integer.toBinaryString(i)).replace(' ', '0');

            for (int j = 0; j < n; j++) {
                if (binaryString.charAt(j) == '1') {
                    aktuelleKombination[j] = items.get(j);
                    totalerWert += items.get(j).getWert();
                    totalesGewicht += items.get(j).getGewicht();
                }
            }

            if (totalesGewicht <= rucksack.getRucksackplatz()
                    && totalerWert > hoechsterWert) {
                besteKombination = aktuelleKombination;
                hoechsterWert = totalerWert;
            }
        }

        //Statistik
        long endZeit = System.currentTimeMillis();
        zeitZumLoesen = endZeit - startZeit;

        return besteKombination;
    }


    // Divide and Conquer Algorithmus
    public Item[] algorithm2(Rucksack rucksack, List<Item> items) {

        //Statistik
        long startZeit = System.currentTimeMillis();
        ConquerBaumInListenform = new ArrayList<>();

        Item[] result = DivideAndConquer(rucksack, items,
                0, items.size() - 1);

        //Statistik
        long endZeit = System.currentTimeMillis();
        zeitZumLoesen = endZeit - startZeit;

        versuchteKombinationen = 0; // Es gibt keine Möglichkeit, die Anzahl der
        // versuchten Kombinationen zu vergleichen, da die Listen unterschiedliche Längen haben.
        // Da das Aufrufen von Algorithm1 aber dafür sorgt, dass versuchteKombinationen hochgezählt wird,
        // wird es hier auf 0 gesetzt, um Verwirrung zu vermeiden.

        return result;
    }

    private Item[] DivideAndConquer(Rucksack rucksack, List<Item> items, int start, int end) {

        if (start > end) {
            return new Item[0];
        } else if (start == end) {
            return new Item[]{items.get(start)};
        }

        //Teilen
        int mid = (start + end) / 2;

        List<Item> haelfteLinks = items.subList(start, mid + 1);
        List<Item> haelfteRechts = items.subList(mid + 1, end + 1);


        Item[] besteKombinationLinks = DivideAndConquer(rucksack,
                haelfteLinks, 0, haelfteLinks.size() - 1);

        Item[] besteKombinationRechts = DivideAndConquer(rucksack,
                haelfteRechts, 0, haelfteRechts.size() - 1);

        //Herrschen
        List<Item> moeglicheItemsListe = new ArrayList<>();
        for (Item item : besteKombinationLinks) {
            if (item != null) {
                moeglicheItemsListe.add(item);
            }
        }
        for (Item item : besteKombinationRechts) {
            if (item != null) {
                moeglicheItemsListe.add(item);
            }
        }

        Item[] loesung = algorithm1(rucksack, moeglicheItemsListe);
        ConquerBaumInListenform.add(loesung);
        return loesung;
    }


    // Dynamische Programmierung
    public Item[] algorithm3(Rucksack rucksack, List<Item> items) {

        //Statistik
        long startZeit = System.currentTimeMillis();

        //besteKombinationMatrix wird initialisiert
        besteKombinationMatrix = new int[items.size()][rucksack.getRucksackplatz() + 1];

        int n = items.size();
        int b = rucksack.getRucksackplatz();
        matrix = new int[n + 1][b + 1];


        for (int i = 0; i <= n; i++) {
            for (int gewicht = 0; gewicht <= b; gewicht++) {
                if (i == 0 || gewicht == 0)
                    matrix[i][gewicht] = 0;
                else if (items.get(i - 1).getGewicht() <= gewicht)
                    // Wenn das aktuelle Item aufgenommen werden kann, wählt es den maximalen Wert aus, wenn es aufgenommen wird oder nicht
                    matrix[i][gewicht] =
                            Math.max(items.get(i - 1).getWert()
                                            + matrix[i - 1]
                                            [gewicht - items.get(i - 1).getGewicht()],
                                    matrix[i - 1][gewicht]);
                else
                    // Wenn das aktuelle item nicht aufgenommen werden kann, übernimmt es den Wert ohne das Item
                    matrix[i][gewicht] = matrix[i - 1][gewicht];
            }
        }

        int maximalerWert = matrix[n][b];
        int gewichtRucksack = b;
        Item[] besteKombination = new Item[n];

        for (int i = n; i > 0 && maximalerWert > 0; i--) {
            if (maximalerWert != matrix[i - 1][gewichtRucksack]) {
                //fürs markieren der besten Kombination im detail Screen
                besteKombinationMatrix[i - 1][gewichtRucksack] = 1;

                besteKombination[i - 1] = items.get(i - 1);
                maximalerWert -= items.get(i - 1).getWert();
                gewichtRucksack -= items.get(i - 1).getGewicht();
            }
        }

        //Statistik
        long endZeit = System.currentTimeMillis();
        zeitZumLoesen = endZeit - startZeit;

        return besteKombination; // Die beste Kombination von Elementen wird ausgegeben
    }


    // Greedy Algorithmus
    public Item[] algorithm4(Rucksack rucksack, List<Item> items) {

        //Statistik
        long startZeit = System.currentTimeMillis();

        int n = items.size();
        sortierteItems = Bubblesort(items);

        Item[] besteKombination = new Item[n];
        int totalesGewicht = 0;
        int index = 0;


        // Durchläuft die sortierte Liste der Artikel
        for (int i = 0; i < n; i++) {
            Item item = sortierteItems[i];
            int itemGewicht = item.getGewicht();

            // Fügt das aktuelle Item zur aktuellen Kombination hinzu, solange
            // das Gesamtgewicht die Kapazität des Rucksacks nicht überschreitet
            if (totalesGewicht + itemGewicht <= rucksack.getRucksackplatz()) {
                besteKombination[index] = item;
                totalesGewicht += itemGewicht;
                index++;
            }
            if (totalesGewicht == rucksack.getRucksackplatz()) {
                break;
            }
        }

        //Statistik
        long endZeit = System.currentTimeMillis();
        zeitZumLoesen = endZeit - startZeit;

        return besteKombination;
    }

    private Item[] Bubblesort(List<Item> items) {
        int n = items.size();
        Item[] zuSortierendeItems = items.toArray(new Item[n]);

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                double ratio1 = zuSortierendeItems[j].getWert() /
                        (double) zuSortierendeItems[j].getGewicht();
                double ratio2 = zuSortierendeItems[j + 1].getWert() /
                        (double) zuSortierendeItems[j + 1].getGewicht();
                if (ratio1 < ratio2) {
                    Item temp = zuSortierendeItems[j];
                    zuSortierendeItems[j] = zuSortierendeItems[j + 1];
                    zuSortierendeItems[j + 1] = temp;
                }
            }
        }
        return zuSortierendeItems;
    }

    // Backtracking Algorithmus
    public Item[] algorithm5(Rucksack rucksack, List<Item> items) {

        //Statistik
        long startZeit = System.currentTimeMillis();

        int n = items.size();
        Item[] besteKombination = new Item[n];
        hoechsterWertB = 0;

        tree = new BinaryTree<>(new Item[0]);

        // Startet den Backtracking-Prozess
        backtracking(rucksack, items, besteKombination, new Item[n],
                0, 0, 0, tree);


        //Statistik
        long endZeit = System.currentTimeMillis();
        zeitZumLoesen = endZeit - startZeit;

        return besteKombination;
    }

    // private Methode um Rekursion zu ermöglichen
    private void backtracking(Rucksack rucksack, List<Item> items, Item[] besteKombination,
                              Item[] aktuelleKombination, int index,
                              int totalerWert, int totalesGewicht, BinaryTree<Item[]> tree) {

        // Wenn das Gesamtgewicht die Kapazität des Rucksacks überschreitet, wird die aktuelle Kombination verworfen
        if (totalesGewicht > rucksack.getRucksackplatz()) {
            return;
        }

        // Wenn der GesamtWert höher ist als der bisher höchste Wert, wird die aktuelle Kombination als die beste Kombination aktualisiert
        if (totalerWert > hoechsterWertB) {
            hoechsterWertB = totalerWert;
            System.arraycopy(aktuelleKombination, 0, besteKombination,
                    0, aktuelleKombination.length);
        }

        // Wenn alle Items durchlaufen wurden, wird der Backtracking-Prozess beendet
        if (index == items.size()) {
            return;
        }

        // Fügt das aktuelle Item zur aktuellen Kombination hinzu und ruft die Backtracking-Methode für das nächste Item auf
        Item item = items.get(index);
        aktuelleKombination[index] = item;

        // Erstellt einen neuen BinaryTree für den linken Teilbaum
        BinaryTree<Item[]> leftChild = new BinaryTree<>(aktuelleKombination.clone());
        tree.setLeftTree(leftChild);

        backtracking(rucksack, items, besteKombination,
                aktuelleKombination, index + 1,
                totalerWert + item.getWert(),
                totalesGewicht + item.getGewicht(), leftChild);


        // Entfernt das aktuelle Item aus der aktuellen Kombination und ruft die Backtracking-Methode für das nächste Item auf
        aktuelleKombination[index] = null;

        // Erstellt einen neuen BinaryTree für den rechten Teilbaum
        BinaryTree<Item[]> rightChild = new BinaryTree<>(aktuelleKombination.clone());
        tree.setRightTree(rightChild);

        backtracking(rucksack, items, besteKombination,
                aktuelleKombination, index + 1,
                totalerWert, totalesGewicht, rightChild);
    }

    //Methode um den GesamtWert einer Kombination zu berechnen
    public int getTotalerWert(Item[] items) {
        int totalerWert = 0;
        if (items == null) return 0;

        for (Item item : items) {
            if (item != null) {
                totalerWert += item.getWert();
            }
        }
        return totalerWert;
    }

}