package de.kilian.demacia.probleme.rucksack;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class VariablenManager {
    private static VariablenManager instance;
    private final Rucksack rucksack;
    private final List<Item> items;
    private final Solver solver = new Solver();

    private VariablenManager() {
        rucksack = new Rucksack(10);
        items = new ArrayList<>();
    }

    public static VariablenManager getInstance() {
        if (instance == null) {
            instance = new VariablenManager();
        }
        return instance;
    }

}
