package de.kilian.demacia.probleme.rucksack;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.Getter;
import lombok.Setter;

import java.util.Random;

@Setter
@Getter
public class Item {

    private int gewicht; // In kg
    private int Wert; // In Euro

    public Item(int gewicht, int Wert) {
        this.gewicht = gewicht;
        this.Wert = Wert;
    }

    public static Item generateRandomItem() {
        return new Item(new Random().nextInt(9) + 1, new Random().nextInt(100) + 1);
    }

    public ImageView getImage() {
        return new ImageView(new Image("Images/rucksack/item" + gewicht + ".png"));
    }

    public boolean isInArray(Item[] items) {
        for (Item item : items) {
            if (this.equals(item)) {
                return true;
            }
        }
        return false;
    }
}