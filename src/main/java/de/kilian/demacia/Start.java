package de.kilian.demacia;

import de.kilian.demacia.views.rucksackscreen.RucksackScreenView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Start extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        // Erstellt ein neues Fenster (den RucksackScreen) mit einer Größe von 1100x550 Pixeln und zeigt es an
        Scene scene = new Scene(new RucksackScreenView().getView(), 1100, 550);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
}