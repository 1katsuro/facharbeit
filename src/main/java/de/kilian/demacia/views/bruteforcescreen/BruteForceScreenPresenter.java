package de.kilian.demacia.views.bruteforcescreen;

import de.kilian.demacia.probleme.rucksack.Item;
import de.kilian.demacia.views.BasisPresenter.BasisPresenter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class BruteForceScreenPresenter extends BasisPresenter implements Initializable {

    @FXML
    private Label versuchteKombinationenLabel, zeitZumLoesenLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (Item item : variablenManager.getItems()) {
            showItem(item, itemsPane, false);
        }
        rucksackLabel.setText("Rucksack (" + variablenManager.getRucksack().getRucksackplatz() + " KG)");
    }

    public void onSolveButtonClicked() {
        Item[] besteKombination = variablenManager.getSolver().algorithm1(
                variablenManager.getRucksack(), variablenManager.getItems());

        updateRucksackPane(besteKombination);
        updateItemsPane(besteKombination);

        versuchteKombinationenLabel.setText("Versuchte Kombinationen: " +
                variablenManager.getSolver().getVersuchteKombinationen());
        zeitZumLoesenLabel.setText("Zeit zum Lösen: " +
                variablenManager.getSolver().getZeitZumLoesen() / 1000 + " Sekunden");
    }
}