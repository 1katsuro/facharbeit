package de.kilian.demacia.views.divideandconquer;

import de.kilian.demacia.datenstrukturen.BinaryTree;
import de.kilian.demacia.probleme.rucksack.Item;
import de.kilian.demacia.views.BasisPresenter.BasisPresenter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class DivideScreenPresenter extends BasisPresenter implements Initializable {

    @FXML
    private Pane dividePane, conquerPane;
    @FXML
    private Group group1, group2;

    private BinaryTree<Item[]> divideBaum, conquerBaum;

    private List<Integer> sizes;

    private final Translate pan = new Translate(); // Transformation für das Verschieben der Ansicht
    private final Scale zoom = new Scale(); // Transformation für das Zoomen der Ansicht
    private double lastX = 0; // Letzte X-Position der Maus
    private double lastY = 0; // Letzte Y-Position der Maus

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Fügt die Zoom- und Pan-Transformationen zur Gruppe hinzu
        group1.getTransforms().addAll(zoom, pan);
        group2.getTransforms().addAll(zoom, pan);

        // Fügt die Scroll- und Drag-Event-Handler hinzu
        dividePane.setOnScroll(this::handleScroll);
        dividePane.setOnMousePressed(this::handleMousePress);
        dividePane.setOnMouseDragged(this::handleMouseDrag);

        conquerPane.setOnScroll(this::handleScroll);
        conquerPane.setOnMousePressed(this::handleMousePress);
        conquerPane.setOnMouseDragged(this::handleMouseDrag);
    }

    public void onAnzeigenButtonClicked() {
        // Führt den Algorithmus aus und erstellt den Baum
        variablenManager.getSolver().algorithm2(variablenManager.getRucksack(),
                variablenManager.getItems());

        sizes = new ArrayList<Integer>();

        erstelleDivideBaum();
        printBaum(divideBaum, dividePane.getWidth() / 2, 50, dividePane);


        erstelleConquerBaum();
        printBaum(conquerBaum, conquerPane.getWidth() / 2, 50, conquerPane);

    }

    private void printBaum(BinaryTree<Item[]> baum, double x, double y, Pane pane) {
        if (!baum.isEmpty()) {

            Item[] items = baum.getContent();
            if (items == null) return;

            int breite;
            if (pane == dividePane) {
                breite = items.length * 20;
                sizes.add(items.length * 20);
            } else {
                breite = sizes.get(0);
                sizes.remove(0);
            }
            // Erstellt ein Rechteck für den aktuellen Knoten
            Rectangle rectangle = new Rectangle(x - (double) breite / 2, y, breite, 25); // (StartX, StartY, Breite, Höhe)
            rectangle.setFill(Color.WHITE);
            rectangle.setStroke(Color.BLACK);

            // Fügt das Rechteck zur Pane hinzu
            pane.getChildren().add(rectangle);

            // Erstellt eine ImageView für jedes Item im Knoten
            for (int i = 0; i < items.length; i++) {
                if (items[i] != null) {
                    ImageView imageView = items[i].getImage();
                    imageView.setLayoutX(x - (double) breite / 2 + i * 20); // Positioniert die Bilder korrekt
                    imageView.setLayoutY(y);
                    imageView.setFitWidth(20); // Ändert die Größe der Bilder
                    imageView.setFitHeight(20); // Ändert die Größe der Bilder

                    // Fügt die ImageView zur Pane hinzu
                    pane.getChildren().add(imageView);
                }
            }

            // Ruft die Methode printBaum() rekursiv für den linken und rechten Teilbaum auf
            if (baum.getLeftTree() != null && !baum.getLeftTree().isEmpty()) {
                // Zeichnet eine Linie zum linken Kindknoten
                Line line = new Line(x, y + 25, x - breite, y + 100); // (StartX, StartY, EndX, EndY)
                pane.getChildren().add(line);
                printBaum(baum.getLeftTree(), x - breite, y + 100, pane);
            }
            if (baum.getRightTree() != null && !baum.getRightTree().isEmpty()) {
                // Zeichnet eine Linie zum rechten Kindknoten
                Line line = new Line(x, y + 25, x + breite, y + 100); // (StartX, StartY, EndX, EndY)
                pane.getChildren().add(line);
                printBaum(baum.getRightTree(), x + breite, y + 100, pane);
            }
        }
    }

    public void erstelleDivideBaum() {
        List<Item> items = variablenManager.getItems();
        divideBaum = new BinaryTree<>(items.toArray(new Item[0]));
        divide(items, divideBaum, 0, items.size() - 1);
    }

    public void erstelleConquerBaum() {
        if (divideBaum != null && !divideBaum.isEmpty()) {
            conquerBaum = divideBaum;
            conquer(conquerBaum);
        }
    }

    private void divide(List<Item> items, BinaryTree<Item[]> teilbaum, int start, int end) {
        if (start > end) {
            teilbaum.setContent(new Item[0]);
            return;
        } else if (start == end) {
            teilbaum.setContent(new Item[]{items.get(start)});
            return;
        }

        int mid = (start + end) / 2;

        List<Item> haelfteLinks = items.subList(start, mid + 1);
        List<Item> haelfteRechts = items.subList(mid + 1, end + 1);


        BinaryTree<Item[]> leftTree = new BinaryTree<>(haelfteLinks.toArray(new Item[0]));
        BinaryTree<Item[]> rightTree = new BinaryTree<>(haelfteRechts.toArray(new Item[0]));

        divide(items, leftTree, start, mid); // Teilt die linke Hälfte der Liste
        divide(items, rightTree, mid + 1, end); // Teilt die rechte Hälfte der Liste

        teilbaum.setLeftTree(leftTree);
        teilbaum.setRightTree(rightTree);
    }

    public void conquer(BinaryTree<Item[]> teilbaum) {
        // Wenn der aktuelle Knoten ein Blatt ist, überspringen Sie die Aktualisierung
        if (teilbaum.getLeftTree().isEmpty() && teilbaum.getRightTree().isEmpty()) {
            return;
        }

        List<Item[]> liste = variablenManager.getSolver().getConquerBaumInListenform();
        teilbaum.setContent(liste.get(liste.size() - 1));
        liste.remove(liste.size() - 1);

        conquer(teilbaum.getRightTree());
        conquer(teilbaum.getLeftTree());
    }


    private void handleScroll(ScrollEvent event) {
        // Behandelt das Scroll-Event
        event.consume();

        double delta = event.getDeltaY();
        double scaleFactor = (delta > 0) ? 1.1 : 1 / 1.1;

        // Skaliert und verschiebt den Zoom, um den Drehpunkt an der gleichen Bildschirmposition zu halten
        double mouseX = event.getX() - (zoom.getPivotX() - pan.getX()) * zoom.getX();
        double mouseY = event.getY() - (zoom.getPivotY() - pan.getY()) * zoom.getY();
        zoom.setPivotX(mouseX);
        zoom.setPivotY(mouseY);
        zoom.setX(zoom.getX() * scaleFactor);
        zoom.setY(zoom.getY() * scaleFactor);
    }

    private void handleMousePress(MouseEvent event) {
        // Speichert die aktuelle Mausposition
        lastX = event.getX();
        lastY = event.getY();
    }

    private void handleMouseDrag(MouseEvent event) {
        // Berechnet die bewegte Distanz und aktualisiert die Pan-Transformation
        double deltaX = event.getX() - lastX;
        double deltaY = event.getY() - lastY;
        pan.setX(pan.getX() + deltaX);
        pan.setY(pan.getY() + deltaY);

        // Aktualisiert die letzte Position für das nächste Mausbewegungsereignis
        lastX = event.getX();
        lastY = event.getY();
    }
}
