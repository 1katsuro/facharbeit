package de.kilian.demacia.views.dynamischeprogrammierung;

import de.kilian.demacia.views.BasisPresenter.BasisPresenter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class DynamischScreenPresenter extends BasisPresenter implements Initializable {

    @FXML
    private Label iLabel, bLabel;
    @FXML
    private Pane matrixPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    // Methode, die aufgerufen wird, wenn der "Lösen" Button geklickt wird
    public void onSolveButtonClicked() {
        // Methode wird aufgerufen, um Matrix zu erstellen/ zu aktualisieren
        variablenManager.getSolver().algorithm3(variablenManager.getRucksack(),
                variablenManager.getItems());

        // Lässt die Matrix anzeigen
        printMatrix(variablenManager.getSolver().getMatrix());

        iLabel.setText("i = " + variablenManager.getItems().size());
        bLabel.setText("b = " + variablenManager.getRucksack().getRucksackplatz());
    }

    private void printMatrix(int[][] matrix) {
        if (matrix == null) {
            System.out.println("Matrix ist null");
            return;
        }

        matrixPane.getChildren().clear();

        // findet das größte Element in der Matrix
        int maxElement = 0;
        for (int[] reihe : matrix) {
            for (int zelle : reihe) {
                if (zelle > maxElement) {
                    maxElement = zelle;
                }
            }
        }

        // berechnet die Boxgröße basierend auf der Länge des größten Elements
        if (maxElement == 0) {
            maxElement = 1;
        }
        int boxSize = String.valueOf(maxElement).length() * 20;

        int x = boxSize;
        int y = boxSize;

        // Fügt Spaltennummern hinzu
        for (int i = 0; i < matrix[0].length; i++) {
            Label label = new Label(String.valueOf(i));
            label.setLayoutX(x + i * boxSize);
            label.setLayoutY(0); // Positioniert die Labels am oberen Rand
            matrixPane.getChildren().add(label);
        }

        // Fügt Reihennummern hinzu
        for (int i = 0; i < matrix.length; i++) {
            Label label = new Label(String.valueOf(i));
            label.setLayoutX(0); // Positioniert die Labels am linken Rand
            label.setLayoutY(y + i * boxSize);
            matrixPane.getChildren().add(label);
        }

        int[][] besteKombinationMatrix = variablenManager.getSolver().getBesteKombinationMatrix();

        int index = 0;
        for (int[] row : matrix) {
            int cellIndex = 0;
            for (int cell : row) {
                Label label = new Label(String.valueOf(cell));
                label.setLayoutX(x);
                label.setLayoutY(y);
                label.setMinWidth(boxSize);
                label.setMinHeight(boxSize);
                label.setStyle("-fx-font-size: 15px; -fx-padding: 10px;" +
                        " -fx-border-color: black; -fx-border-width: 1px; -fx-alignment: center;");

                // Wenn die Zelle zur besten Kombination gehört, umranden Sie sie rot
                if (index < besteKombinationMatrix.length
                        && cellIndex < besteKombinationMatrix[index].length
                        && besteKombinationMatrix[index][cellIndex] == 1) {
                    label.setStyle(label.getStyle() + " -fx-border-color: red;"
                            + " -fx-border-width: 2px;"
                            + " -fx-text-fill: red;");
                }

                matrixPane.getChildren().add(label);
                x += boxSize;
                cellIndex++;
            }
            // Größe der Pane aktualisieren
            matrixPane.setPrefWidth(x + boxSize);
            matrixPane.setPrefHeight(y + 2 * boxSize);

            x = boxSize; // Zurücksetzen der x-Position für die nächste Zeile
            y += boxSize;
            index++;
        }
    }
}
