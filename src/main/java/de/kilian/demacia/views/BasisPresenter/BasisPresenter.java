package de.kilian.demacia.views.BasisPresenter;

import de.kilian.demacia.probleme.rucksack.Item;
import de.kilian.demacia.probleme.rucksack.VariablenManager;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public abstract class BasisPresenter {

    @FXML
    protected GridPane itemsPane, rucksackPane;
    @FXML
    protected Label rucksackLabel;

    protected VariablenManager variablenManager = VariablenManager.getInstance();


    public void onAddItemButtonClicked() {
        // Erstellt ein Dialogfenster, in dem der Benutzer das Gewicht und den Wert des neuen Items eingeben kann
        Dialog<Item> dialog = new Dialog<>();
        dialog.setTitle("Add Item");
        dialog.setHeaderText("Enter the gewicht and Wert:");

        // Erstellt Labels und TextFields für das Gewicht und den Wert
        Label gewichtLabel = new Label("Gewicht:");
        Label WertLabel = new Label("Wert:");
        TextField gewichtField = new TextField();
        TextField WertField = new TextField();


        // Erstellt ein GridPane und fügt die Labels und Fields hinzu
        GridPane grid = new GridPane();
        grid.add(gewichtLabel, 1, 1);
        grid.add(gewichtField, 2, 1);
        grid.add(WertLabel, 1, 2);
        grid.add(WertField, 2, 2);
        dialog.getDialogPane().setContent(grid);

        // Erstellt einen OK-Button und fügt ihn zum Dialogfenster hinzu
        ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(okButtonType);

        // Setzt den ResultConverter, um das Item zu erstellen, wenn der Benutzer auf OK klickt
        dialog.setResultConverter(buttonType -> {
            if (buttonType == okButtonType && !gewichtField.getText().isEmpty()
                    && !WertField.getText().isEmpty()
                    && gewichtField.getText().matches("\\d+")
                    && WertField.getText().matches("\\d+")) {
                int gewicht = Integer.parseInt(gewichtField.getText());
                int Wert = Integer.parseInt(WertField.getText());

                if (gewicht > 9) gewicht = 9;
                else if (gewicht < 1) gewicht = 1;
                if (Wert > 100) Wert = 100;
                else if (Wert < 1) Wert = 1;
                return new Item(gewicht, Wert);
            }
            return null;
        });

        // Zeigt das Dialogfenster an und wartet auf die Eingabe des Benutzers
        Item result = dialog.showAndWait().orElse(null);
        if (result != null) {
            // Fügt das neue Item zur Liste hinzu
            variablenManager.getItems().add(result);
            // zeigt das neue Item in der itemsPane
            showItem(result, itemsPane, false);
        }
    }

    public void onRandomItemButtonClicked() {
        // Erstellt ein Item mit zufälligem Gewicht und Wert
        Item randomItem = Item.generateRandomItem();
        // Fügt das neue Item zur Liste hinzu
        variablenManager.getItems().add(randomItem);
        //zeigt das neue Item in der itemsPane
        showItem(randomItem, itemsPane, false);
    }

    public void onRemoveItemButtonClicked() {
        // Löscht das letzte Item aus der Liste
        if (!variablenManager.getItems().isEmpty()) {
            variablenManager.getItems().remove(variablenManager.getItems().size() - 1);
            if (!itemsPane.getChildren().isEmpty()) {
                itemsPane.getChildren().remove(itemsPane.getChildren().size() - 1);
            }
        }
    }


    public void onSetGewichtClicked() {
        // Erstellt ein Dialogfenster, in dem der Benutzer das Gewicht des Rucksacks eingeben kann
        TextInputDialog dialog = new TextInputDialog("" + variablenManager.getRucksack().getRucksackplatz());
        dialog.setTitle("Add Gewicht");
        dialog.setHeaderText("Enter the gewicht of the Rucksack:");
        dialog.setContentText("Gewicht:");

        // Zeigt das Dialogfenster an und wartet auf die Eingabe des Benutzers
        dialog.showAndWait().ifPresent(gewicht -> {
            if (!gewicht.matches("\\d+") || gewicht.isEmpty()) {
                return;
            }
            int nRucksackGewicht = Integer.parseInt(gewicht);
            if (nRucksackGewicht < 1) nRucksackGewicht = 1;
            else if (nRucksackGewicht > 100) nRucksackGewicht = 100;
            variablenManager.getRucksack().setRucksackplatz(nRucksackGewicht);

            if (rucksackLabel != null) {
                rucksackLabel.setText("Rucksack (" + variablenManager.getRucksack().getRucksackplatz() + " KG)");
            }
        });
    }

    public void onAddGewichtClicked() {
        variablenManager.getRucksack().setRucksackplatz(variablenManager.getRucksack().getRucksackplatz() + 1);
        if (rucksackLabel != null) {
            rucksackLabel.setText("Rucksack (" + variablenManager.getRucksack().getRucksackplatz() + " KG)");
        }
    }

    public void onRemoveGewichtClicked() {
        if (variablenManager.getRucksack().getRucksackplatz() == 0) return;

        variablenManager.getRucksack().setRucksackplatz(variablenManager.getRucksack().getRucksackplatz() - 1);
        if (rucksackLabel != null) {
            rucksackLabel.setText("Rucksack (" + variablenManager.getRucksack().getRucksackplatz() + " KG)");
        }
    }


    protected void updateRucksackPane(Item[] besteKombination) {
        // Löscht alle Items aus dem rucksackPane
        rucksackPane.getChildren().clear();

        // Fügt die besten Items zum rucksackPane hinzu
        if (besteKombination != null) {
            for (Item item : besteKombination) {
                if (item != null) {
                    showItem(item, rucksackPane, false);
                }
            }
        }
        updateItemsPane(besteKombination);
    }

    protected void updateItemsPane(Item[] besteKombination) {
        // Löscht alle Items aus dem itemsPane
        itemsPane.getChildren().clear();

        // Fügt alle Items zur itemsPane hinzu
        for (Item item : variablenManager.getItems()) {
            if (item != null) {
                boolean teilDerBestenKombination = besteKombination != null
                        && item.isInArray(besteKombination);
                showItem(item, itemsPane, teilDerBestenKombination);
            }
        }
    }

    protected void showItem(Item item, GridPane pane, boolean teilDerBestenKombination) {

        VBox vBox = getItemVBox(item);
        vBox.setAlignment(Pos.CENTER); // Zentriert die Elemente in der VBox

        // Wenn das Item zur besten Kombination gehört, umranden Sie es rot
        if (teilDerBestenKombination) {
            vBox.setStyle("-fx-border-color: red; -fx-border-width: 2px;");
        }

        // Sorgt dafür, dass die Items alle nebeneinander (in einem Grid) angezeigt werden
        int row = pane.getChildren().size() / 7;
        int column = pane.getChildren().size() % 7;
        pane.add(vBox, column, row);


    }

    private static VBox getItemVBox(Item item) {
        ImageView imageView = item.getImage(); // Erstellt ein ImageView mit dem Bild des Items

        Label itemGewichtLabel = new Label("Gewicht: " + item.getGewicht());
        Label itemWertLabel = new Label("Wert: " + item.getWert());

        imageView.setFitWidth(35); // Setzt die Breite des Bildes auf 32
        imageView.setFitHeight(35); // Setzt die Höhe des Bildes auf 32

        itemWertLabel.setStyle("-fx-font-size: 10;"); // Setzt die Schriftgröße auf 12
        itemGewichtLabel.setStyle("-fx-font-size: 10;"); // Setzt die Schriftgröße auf 12

        return new VBox(imageView, itemGewichtLabel, itemWertLabel);
    }


}




