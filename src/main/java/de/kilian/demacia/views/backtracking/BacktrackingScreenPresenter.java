package de.kilian.demacia.views.backtracking;

import de.kilian.demacia.datenstrukturen.BinaryTree;
import de.kilian.demacia.probleme.rucksack.Item;
import de.kilian.demacia.views.BasisPresenter.BasisPresenter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class BacktrackingScreenPresenter extends BasisPresenter implements Initializable {

    @FXML
    private Pane treePane;
    @FXML
    private Group group;

    private int breite; // Breite der Rechtecke
    private Item[] besteKombination;

    private final Translate pan = new Translate();
    private final Scale zoom = new Scale();
    private double lastX = 0;
    private double lastY = 0;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Fügt die Zoom- und Pan-Transformationen zur Gruppe hinzu
        group.getTransforms().addAll(zoom, pan);

        // Fügt die Scroll- und Drag-Event-Handler hinzu
        treePane.setOnScroll(this::handleScroll);
        treePane.setOnMousePressed(this::handleMousePress);
        treePane.setOnMouseDragged(this::handleMouseDrag);

    }

    // Methode, die aufgerufen wird, wenn der "Anzeigen" Button geklickt wird
    public void onAnzeigenButtonClicked() {

        // Methode wird aufgerufen, um den Baum zu erstellen/ zu aktualisieren
        variablenManager.getSolver().algorithm5(variablenManager.getRucksack(),
                variablenManager.getItems());

        // Holt den Baum aus dem Solver
        BinaryTree<Item[]> tree = variablenManager.getSolver().getTree();

        // Lässt den Baum anzeigen
        besteKombination = variablenManager.getSolver().algorithm5(variablenManager.getRucksack(),
                variablenManager.getItems());
        breite = variablenManager.getItems().size() * 20;
        printBaum(tree, treePane.getWidth() / 2, 50, treePane, berechneTiefe(tree) - 1);
    }

    private int berechneTiefe(BinaryTree<Item[]> baum) {
        if (baum.isEmpty()) {
            return 0;
        } else {
            return 1 + Math.max(berechneTiefe(baum.getLeftTree()), berechneTiefe(baum.getRightTree()));
        }
    }

    private void printBaum(BinaryTree<Item[]> baum, double x, double y, Pane pane, int level) {
        if (!baum.isEmpty()) {

            Item[] items = baum.getContent();
            if (items == null) return;
            // Erstellt ein Rechteck für den aktuellen Knoten
            Rectangle rectangle = getRectangle(x, y, items);

            // Fügt das Rechteck zur Pane hinzu
            pane.getChildren().add(rectangle);

            // Erstellt eine ImageView für jedes Item im Knoten
            for (int i = 0; i < items.length; i++) {
                if (items[i] != null) {
                    ImageView imageView = items[i].getImage();
                    imageView.setLayoutX(x - (double) breite / 2 + i * 20); // Positioniert die Bilder korrekt
                    imageView.setLayoutY(y);
                    imageView.setFitWidth(20); // Ändert die Größe der Bilder
                    imageView.setFitHeight(20); // Ändert die Größe der Bilder

                    // Fügt die ImageView zur Pane hinzu
                    pane.getChildren().add(imageView);
                }
            }

            // Ruft die Methode printBaum() rekursiv für den linken und rechten Teilbaum auf
            if (baum.getLeftTree() != null && !baum.getLeftTree().isEmpty()) {
                // Zeichnet eine Linie zum linken Kindknoten
                Line line = new Line(x, y + 25, x - breite * Math.pow(2, level), y + 100); // (StartX, StartY, EndX, EndY)
                pane.getChildren().add(line);
                printBaum(baum.getLeftTree(), x - breite * Math.pow(2, level), y + 100, pane, level - 1);
            }
            if (baum.getRightTree() != null && !baum.getRightTree().isEmpty()) {
                // Zeichnet eine Linie zum rechten Kindknoten
                Line line = new Line(x, y + 25, x + breite * Math.pow(2, level), y + 100); // (StartX, StartY, EndX, EndY)
                pane.getChildren().add(line);
                printBaum(baum.getRightTree(), x + breite * Math.pow(2, level), y + 100, pane, level - 1);
            }
        }
    }

    private Rectangle getRectangle(double x, double y, Item[] items) {
        Rectangle rectangle = new Rectangle(x - (double) breite / 2, y, breite, 25); // (StartX, StartY, Breite, Höhe)
        // Überprüft, ob die Kombination des aktuellen Knotens die beste Kombination ist
        if (Arrays.equals(items, besteKombination)) {
            rectangle.setFill(Color.RED);
            rectangle.setStroke(Color.BLACK);
            rectangle.setStrokeWidth(3); // Macht die Linie des Rechtecks dicker
        } else {
            rectangle.setFill(Color.WHITE);
            rectangle.setStroke(Color.BLACK);
        }
        return rectangle; // Gibt das Rechteck zurück
    }

    private void handleScroll(ScrollEvent event) {
        event.consume();

        double delta = event.getDeltaY();
        double scaleFactor = (delta > 0) ? 1.1 : 1 / 1.1;

        // Skaliert und verschiebt den Zoom, um den Drehpunkt an der gleichen Bildschirmposition zu halten
        double mouseX = event.getX() - (zoom.getPivotX() - pan.getX()) * zoom.getX();
        double mouseY = event.getY() - (zoom.getPivotY() - pan.getY()) * zoom.getY();
        zoom.setPivotX(mouseX);
        zoom.setPivotY(mouseY);
        zoom.setX(zoom.getX() * scaleFactor);
        zoom.setY(zoom.getY() * scaleFactor);
    }

    private void handleMousePress(MouseEvent event) {
        // Speichert die aktuelle Mausposition
        lastX = event.getX();
        lastY = event.getY();
    }

    private void handleMouseDrag(MouseEvent event) {
        // Berechnet die bewegte Distanz und aktualisiert die Pan-Transformation
        double deltaX = event.getX() - lastX;
        double deltaY = event.getY() - lastY;
        pan.setX(pan.getX() + deltaX);
        pan.setY(pan.getY() + deltaY);

        // Aktualisiert die letzte Position für das nächste Mausbewegungsereignis
        lastX = event.getX();
        lastY = event.getY();
    }
}