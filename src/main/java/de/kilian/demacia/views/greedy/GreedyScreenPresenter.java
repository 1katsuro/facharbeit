package de.kilian.demacia.views.greedy;

import de.kilian.demacia.probleme.rucksack.Item;
import de.kilian.demacia.views.BasisPresenter.BasisPresenter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.ResourceBundle;

public class GreedyScreenPresenter extends BasisPresenter implements Initializable {

    @FXML
    private GridPane itemsPane, sortiertePane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (Item item : variablenManager.getItems()) {
            showItem(item, itemsPane, false);
        }
    }

    public void onAnzeigenButtonClicked() {

        // Löschen des Inhalts der sortiertenPane
        sortiertePane.getChildren().clear();

        variablenManager.getSolver().algorithm4(
                variablenManager.getRucksack(), variablenManager.getItems());

        Item[] besteKombination = variablenManager.getSolver().algorithm4(
                variablenManager.getRucksack(), variablenManager.getItems());

        for (Item item : variablenManager.getSolver().getSortierteItems()) {
            boolean isBesteKombination = item.isInArray(besteKombination);
            showItem(item, sortiertePane, isBesteKombination);
        }

        updateItemsPane(besteKombination);
    }

}
