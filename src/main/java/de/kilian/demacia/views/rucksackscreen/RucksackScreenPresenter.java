package de.kilian.demacia.views.rucksackscreen;

import de.kilian.demacia.probleme.rucksack.Item;
import de.kilian.demacia.views.BasisPresenter.BasisPresenter;
import de.kilian.demacia.views.backtracking.BacktrackingScreenPresenter;
import de.kilian.demacia.views.bruteforcescreen.BruteForceScreenPresenter;
import de.kilian.demacia.views.divideandconquer.DivideScreenPresenter;
import de.kilian.demacia.views.dynamischeprogrammierung.DynamischScreenPresenter;
import de.kilian.demacia.views.greedy.GreedyScreenPresenter;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RucksackScreenPresenter extends BasisPresenter implements Initializable {


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }


    // Hier wird der Algorithmus aufgerufen, wenn der entsprechende Button geklickt wird
    public void onAlgorithm1ButtonClicked() {
        Item[] besteKombination = variablenManager.getSolver().algorithm1(
                variablenManager.getRucksack(), variablenManager.getItems());
        updateRucksackPane(besteKombination);
        System.out.println("gesamter Wert=" + variablenManager.getSolver().getTotalerWert(besteKombination)
                + ", Zeit in s:" + variablenManager.getSolver().getZeitZumLoesen());
    }

    public void onAlgorithm2ButtonClicked() {
        Item[] besteKombination = variablenManager.getSolver().algorithm2(
                variablenManager.getRucksack(), variablenManager.getItems());
        updateRucksackPane(besteKombination);
        System.out.println("gesamter Wert=" + variablenManager.getSolver().getTotalerWert(besteKombination)
                + ", Zeit in s:" + variablenManager.getSolver().getZeitZumLoesen());
    }

    public void onAlgorithm3ButtonClicked() {
        Item[] besteKombination = variablenManager.getSolver().algorithm3(
                variablenManager.getRucksack(), variablenManager.getItems());
        updateRucksackPane(besteKombination);
        System.out.println("gesamter Wert=" + variablenManager.getSolver().getTotalerWert(besteKombination)
                + ", Zeit in s:" + variablenManager.getSolver().getZeitZumLoesen());
    }

    public void onAlgorithm4ButtonClicked() {
        Item[] besteKombination = variablenManager.getSolver().algorithm4(
                variablenManager.getRucksack(), variablenManager.getItems());
        updateRucksackPane(besteKombination);
        System.out.println("gesamter Wert=" + variablenManager.getSolver().getTotalerWert(besteKombination)
                + ", Zeit in s:" + variablenManager.getSolver().getZeitZumLoesen());
    }

    public void onAlgorithm5ButtonClicked() {
        Item[] besteKombination = variablenManager.getSolver().algorithm5(
                variablenManager.getRucksack(), variablenManager.getItems());
        updateRucksackPane(besteKombination);
        System.out.println("gesamter Wert=" + variablenManager.getSolver().getTotalerWert(besteKombination)
                + ", Zeit in s:" + variablenManager.getSolver().getZeitZumLoesen());
    }

    // Hier wird ein neues Fenster geöffnet, wenn der entsprechende Button geklickt wird
    public void onAlgorithm1DetailsButtonClicked() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(
                "/de/kilian/demacia/views/bruteforcescreen/bruteforcescreen.fxml"));
        loader.setControllerFactory(param -> new BruteForceScreenPresenter());
        Parent root = loader.load();

        Scene scene = new Scene(root, 1100, 550);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    public void onAlgorithm2DetailsButtonClicked() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(
                "/de/kilian/demacia/views/divideandconquer/divideScreen.fxml"));
        loader.setControllerFactory(param -> new DivideScreenPresenter());
        Parent root = loader.load();

        Scene scene = new Scene(root, 1100, 550);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    public void onAlgorithm3DetailsButtonClicked() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(
                "/de/kilian/demacia/views/dynamischeprogrammierung/DynamischScreen.fxml"));
        loader.setControllerFactory(param -> new DynamischScreenPresenter());
        Parent root = loader.load();

        Scene scene = new Scene(root, 1100, 550);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    public void onAlgorithm4DetailsButtonClicked() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(
                "/de/kilian/demacia/views/greedy/greedyScreen.fxml"));
        loader.setControllerFactory(param -> new GreedyScreenPresenter());
        Parent root = loader.load();

        Scene scene = new Scene(root, 1100, 550);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    public void onAlgorithm5DetailsButtonClicked() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(
                "/de/kilian/demacia/views/backtracking/backtrackingScreen.fxml"));
        loader.setControllerFactory(param -> new BacktrackingScreenPresenter());
        Parent root = loader.load();

        Scene scene = new Scene(root, 1100, 550);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }


}






